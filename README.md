CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module allows to trigger slack notifications based on migrate API events.


REQUIREMENTS
------------

This module requires the following modules:

 * Slack - https://www.drupal.org/project/slack


INSTALLATION
------------

 * Install the Slack module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Web Services > Slack >
       Migrate to enable/disable events notifications.
    3. Save configuration.


MAINTAINERS
-----------

 * Ericmaster - https://www.drupal.org/u/ericmaster

Supporting organization:

 * Taoti Creative - https://www.drupal.org/taoti-creative-0
 * Nimblersoft - https://www.drupal.org/nimblersoft
