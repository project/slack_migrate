<?php

namespace Drupal\slack_migrate;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Event\MigrateRowDeleteEvent;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\Event\MigrateMapDeleteEvent;
use Drupal\migrate\Event\MigrateMapSaveEvent;
use Drupal\migrate\Event\MigrateRollbackEvent;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to forward Migrate events to source and destination plugins.
 */
class MigrateEventSubscriber implements EventSubscriberInterface {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Migration counters.
   *
   * @var array
   */
  protected $migration_counters = [];

  /**
   *  Notifies that an import is about to start.
   */
  public function onPreImport(MigrateImportEvent  $event) {
    $migration_id = $event->getMigration()->id();
    $migration_map = $event->getMigration()->getIdMap();
    // Initialize counters.
    $this->migration_counters[$migration_map->getQualifiedMapTableName()] = [
      MigrateIdMapInterface::STATUS_FAILED => 0,
      MigrateIdMapInterface::STATUS_IGNORED => 0,
      MigrateIdMapInterface::STATUS_IMPORTED => 0,
      MigrateIdMapInterface::STATUS_NEEDS_UPDATE => 0,
    ];
    $message = $this->t('Migration @migration started.',
      ['@migration' => $migration_id]);
    \Drupal::service('slack.slack_service')->sendMessage($message);
  }

  /**
   *  Notifies that an import has finished and gives a summary.
   */
  public function onPostImport(MigrateImportEvent  $event) {
    static $sent = [];
    // Check if the message has been already sent.
    $migration_id = $event->getMigration()->id();
    if (!empty($sent[$migration_id])) {
      return;
    }
    $table_name = $migration_map = $event->getMigration()->getIdMap()
      ->getQualifiedMapTableName();
    $processed = $this->migration_counters[$table_name][MigrateIdMapInterface::STATUS_IMPORTED] +
      $this->migration_counters[$table_name][MigrateIdMapInterface::STATUS_FAILED] +
      $this->migration_counters[$table_name][MigrateIdMapInterface::STATUS_IGNORED];
    $singular_message = "Migration @name completed. Processed 1 item (@created created/updated, @failures failed, @ignored ignored)";
    $plural_message = "Migration @name completed. Processed @numitems items (@created created/updated, @failures failed, @ignored ignored)";
    $message = \Drupal::translation()->formatPlural($processed,
      $singular_message, $plural_message,
        [
          '@numitems' => $processed,
          '@created' =>  $this->migration_counters[$table_name][MigrateIdMapInterface::STATUS_IMPORTED],
          // '@updated' => $this->migration_counters[$table_name][MigrateIdMapInterface::STATUS_NEEDS_UPDATE],
          '@failures' => $this->migration_counters[$table_name][MigrateIdMapInterface::STATUS_FAILED],
          '@ignored' => $this->migration_counters[$table_name][MigrateIdMapInterface::STATUS_IGNORED],
          '@name' => $migration_id,
        ]
    )->render();
    \Drupal::service('slack.slack_service')->sendMessage($message);
    $sent[$migration_id] = TRUE;
  }

  /**
   * Count up any map save events.
   *
   * @param \Drupal\migrate\Event\MigrateMapSaveEvent $event
   *   The map event.
   */
  public function onMapSave(MigrateMapSaveEvent $event) {
    // Count events so we can present the results.
    $fields = $event->getFields();
    $table_name = $event->getMap()->getQualifiedMapTableName();
    // @TODO: Distinguish between creation and update.
    $pre_existing = FALSE;
    if ($fields['source_row_status'] == MigrateIdMapInterface::STATUS_IMPORTED && $pre_existing) {
      $this->migration_counters[$table_name][MigrateIdMapInterface::STATUS_NEEDS_UPDATE]++;
    }
    else {
      $this->migration_counters[$table_name][$fields['source_row_status']]++;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[MigrateEvents::PRE_IMPORT][] = ['onPreImport'];
    $events[MigrateEvents::POST_IMPORT][] = ['onPostImport'];
    $events[MigrateEvents::MAP_SAVE][] = ['onMapSave'];
    // $events[MigrateEvents::PRE_ROW_SAVE][] = ['onPreRowSave'];

    // $this->listeners[MigrateEvents::MAP_SAVE] = [$this, 'onMapSave'];
    // $this->listeners[MigrateEvents::MAP_DELETE] = [$this, 'onMapDelete'];
    // $this->listeners[MigrateEvents::POST_IMPORT] = [$this, 'onPostImport'];
    // $this->listeners[MigrateEvents::POST_ROLLBACK] = [$this, 'onPostRollback'];
    // $this->listeners[MigrateEvents::PRE_ROW_SAVE] = [$this, 'onPreRowSave'];
    // $this->listeners[MigrateEvents::POST_ROW_DELETE] = [$this, 'onPostRowDelete'];
    // $this->listeners[MigratePlusEvents::PREPARE_ROW] = [$this, 'onPrepareRow'];

    return $events;
  }

}
