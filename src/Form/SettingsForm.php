<?php

namespace Drupal\slack_migrate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slack_migrate_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['slack_migrate.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('slack_migrate.settings');
    $form['pre_import_notifications'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Pre Import notifications'),
      '#description' => $this->t('Sends a message when a migration has started.'),
      '#default_value' => $config->get('pre_import_notifications'),
      '#weight' => '-99',
    ];
    $form['post_import_notifications'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Post Import notifications'),
      '#description' => $this->t('Sends a message when a migration has completed.'),
      '#default_value' => $config->get('post_import_notifications'),
      '#weight' => '-98',
    ];
    $form['failed_items_notifications'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Failed items notifications'),
      '#description' => $this->t('Sends a message when a row has failed.'),
      '#default_value' => $config->get('failed_items_notifications'),
      '#weight' => '-97',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('slack_migrate.settings');
    $config
      ->set('pre_import_notifications', trim($form_state->getValue('pre_import_notifications')))
      ->set('post_import_notifications', $form_state->getValue('post_import_notifications'))
      ->set('failed_items_notifications', $form_state->getValue('failed_items_notifications'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
